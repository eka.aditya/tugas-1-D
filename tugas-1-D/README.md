Annisa Sri Devi
Eka Aditya Pramudita
Nurfathiya Faradiena Azzahra
Priya Arif Abdul Azis

pipeline status
[![pipeline status](https://gitlab.com/eka.aditya/tugas-1-D/badges/master/pipeline.svg)](https://gitlab.com/eka.aditya/tugas-1-D/commits/master)
<a href="https://gitlab.com/eka.aditya/tugas-1-D/commits/master"><img alt="pipeline status" src="https://gitlab.com/eka.aditya/tugas-1-D/badges/master/pipeline.svg" /></a>
image:https://gitlab.com/eka.aditya/tugas-1-D/badges/master/pipeline.svg[link="https://gitlab.com/eka.aditya/tugas-1-D/commits/master",title="pipeline status"]

coverage report
[![coverage report](https://gitlab.com/eka.aditya/tugas-1-D/badges/master/coverage.svg)](https://gitlab.com/eka.aditya/tugas-1-D/commits/master)
<a href="https://gitlab.com/eka.aditya/tugas-1-D/commits/master"><img alt="coverage report" src="https://gitlab.com/eka.aditya/tugas-1-D/badges/master/coverage.svg" /></a>
image:https://gitlab.com/eka.aditya/tugas-1-D/badges/master/coverage.svg[link="https://gitlab.com/eka.aditya/tugas-1-D/commits/master",title="coverage report"]

https://diligentsquad.herokuapp.com/